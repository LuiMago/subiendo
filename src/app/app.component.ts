import { EmployeeComponent } from './employess/employee/employee.component';
import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';


export interface PeriodicElement {
  name: string;
  position: number;
  mobile: number;
  email: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', mobile: 1.0079, email: 'H'},
  {position: 2, name: 'Helium', mobile: 4.0026, email: 'He'},
  {position: 3, name: 'Lithium', mobile: 6.941, email: 'Li'},
  {position: 4, name: 'Beryllium', mobile: 9.0122, email: 'Be'},
  {position: 5, name: 'Boron', mobile: 10.811, email: 'B'},
  {position: 6, name: 'Carbon', mobile: 12.0107, email: 'C'},
  {position: 7, name: 'Nitrogen', mobile: 14.0067, email: 'N'},
  {position: 8, name: 'Oxygen', mobile: 15.9994, email: 'O'},
  {position: 9, name: 'Fluorine', mobile: 18.9984, email: 'F'},
  {position: 10, name: 'Neon', mobile: 20.1797, email: 'Ne'},
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  displayedColumns: string[] = ['position', 'name', 'mobile', 'email', 'action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA) ;
 /*
  model:any = {};
  model2:any = {};
  hideUpdate:boolean = true;*/

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }



/*
  addEmployee():void{
    this.dataSource.push(this.model);
    //this.msg = 'campo agregado';
  }

  deleteEmployee(i):void {

     this.dataSource.splice(i, 1);
      //this.msg = 'campo eliminado';

  }

  myValue;
  editEmployee(i):void {
    this.hideUpdate = false;
    this.model2.name = this.dataSource[i].name;
    this.model2.position = this.dataSource[i].position;
    this.model2.email = this.dataSource[i].email;
    this.myValue = i;
  }

  updateEmployee():void {
    let i = this.myValue;
    for(let j = 0; j < this.dataSource.length; j++){
      if(i == j) {
        this.dataSource[i] = this.model2;
        //this.msg = 'campo actualizado';
        this.model2 = {};
      }
    }
  } */



}


